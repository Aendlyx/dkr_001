import requests

use_case = {
'url' : 'http://172.28.5.1',
'prt' : '10101',
'rte' : '/public',
'vrb' : "post",
'djs' : {
        'lgn' : 'd4m1',
        'pwd' : 'cypher_that_shitty_password_1234567890',
        'req' : [{
                    'ressources' : 'events',
                    'location'   : '48.870380_2.345782',
                }],
        'res' : [{
                    'event' : 'name',
                    'venue' : 'hoxton',
                }],
    },
}

fns_vrb = {
    'get' : requests.get,
    'post': requests.post,
}

def bld_url(**kw):
    return kw['url'] + ':' + kw['prt'] + kw['rte']
    
def cal_vrb(**kw):
    return fns_vrb[kw['vrb']](url=bld_url(**kw), json=kw['djs'])

calls = []
calls.append(cal_vrb(**use_case))
for call in calls:
    print(f"[ {call.status_code} ] [{call.json()}]")


