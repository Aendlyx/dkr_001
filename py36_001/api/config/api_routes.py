# coding: utf-8

from core.routes.public.get import get_public
from core.routes.public.post import post_public


api_routes = {
	'get_public' : {	'Method'   : 'GET' 		,
	 			   		'Route'    : '/public' ,
	 			   		'Function' : get_public
	},
	'post_public' : {	'Method'   : 'POST' 		,
	 			   		'Route'    : '/public' ,
	 			   		'Function' : post_public
	},
}