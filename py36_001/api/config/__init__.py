# coding:utf-8

from config.api_config import api_config
from config.api_routes import api_routes

__all__ = [
	'api_config',
	'api_routes',
]