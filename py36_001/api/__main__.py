#coding:utf-8
from core import *

if __name__ == '__main__':

	api_config['loop'] = asyncio.get_event_loop()

	api_config['loop'].run_until_complete(aio_api(
			loop=api_config['loop'],
			host=api_config['host'],
			port=api_config['port'],
			routes=api_routes
			).build_api_server())

	print(f"http://{api_config['host']}:{api_config['port']}")

	try:
		api_config['loop'].run_forever()
	except KeyboardInterrupt:
		api_config['loop'].close()
