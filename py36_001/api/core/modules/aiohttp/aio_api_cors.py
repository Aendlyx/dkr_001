#coding:utf-8

import aiohttp_cors

class aio_api_cors():
	def __init__(self, **kw):
		self._kw = kw

		self._kw['defaults_cors'] = {
	    	"*": aiohttp_cors.ResourceOptions(
            	allow_credentials=True,
            	expose_headers="*",
            	allow_headers="*")}

		self.setup_cors()
		self.open_all()

	
	def setup_cors(self):
		self._kw['cors'] = aiohttp_cors.setup(
			self._kw['app'],
			defaults=self._kw['defaults_cors']
		)
		return True


	def open_all(self):
		for route in list(self._kw['app'].router.routes()):
			self._kw['cors'].add(route)
		return True
