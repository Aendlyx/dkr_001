#coding:utf-8

from aiohttp import web
from .aio_api_cors import aio_api_cors

class aio_api():
	def __init__(self, **kw):
			self._kw = kw

	async def build_api_server(self):
		self._kw['app'] = web.Application(loop=self._kw['loop'])
		self.setup_routes()
		aio_api_cors(app=self._kw['app'])
		return await self._kw['loop'].create_server(
			self._kw['app'].make_handler(),
			self._kw['host'],
			self._kw['port']
		)

	def setup_routes(self):
		for route_name , route_config in self._kw['routes'].items():
			self._kw['app'].router.add_route(
				route_config['Method'],
				route_config['Route'],
				route_config['Function']
			)
		return True
