# coding: utf-8
from importlib import import_module

class importer():
	def __init__(self, *args, **kwargs):
		print(f"[class]=> api_importer [function]=> init()")

	def load_one_module(self, *args, **kwargs):
		module_name = kwargs['module_name']
		class_name = kwargs['class_name']
		module = import_module(module_name, package=None)
		module_class = getattr(module, class_name)
		instance = module_class(*args, **kwargs)
		return instance
