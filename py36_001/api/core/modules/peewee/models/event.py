from peewee import *
from __init__ import *

class Event(BaseModel):
    promoter = ForeignKeyField(Promoter, backref='events')
    description = TextField()