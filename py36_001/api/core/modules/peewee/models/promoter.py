from peewee import *
from __init__ import *

class Promoter(BaseModel):
	id_promoter = BigAutoField(primary_key=True, unique=True)
	name = CharField(unique=True)