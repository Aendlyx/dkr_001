# coding : utf-8
# [post.py][001]
from core.routes.availables import routes_availables
from core.routes import *

dfn = {
  'routes_availables' : routes_availables,

}

async def post_public(request):

  try:
    jrep = await request.json()
    print(jrep)
  except Exception as exc:
    return json_response({'404':'give me json'})

  try:
    if jrep['req'] in dfn.keys():
      print(f"{jrep['req']}()")
      return json_response(dfn[jrep['req']]())
  except Exception as exc:
    return HTTPNotAcceptable()
