# coding : utf-8
# [availables.py][001]
from core.routes import json_response
from core.classes.route_kw import route_kw

def routes_availables():
  public_kw = { '_route_' : '/public',
                '_verbs_' : { '_get_'   : True,
                              '_post_'  : True, } }
  public = route_kw(**public_kw)
  return public.g_kw()