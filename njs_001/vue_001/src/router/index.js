import Vue from 'vue'
import Router from 'vue-router'
import Twist from '@/components/Twist'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Twist',
      component: Twist
    }
  ]
})
